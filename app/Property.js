/** @typedef {import('./Player.js').default} Player */
/** @typedef {import('./Turn.js').default} Turn */
/** @typedef {import('./Game.js').default} Game */

import Space from './Space.js';

export const PropertyType = {
    STREET: 0,
    RAILROAD_STATION: 1,
    UTILITY: 2
};


export default class Property extends Space {

    /** @type {number} */
    property_type;

    /** @type {number} */
    value;

    /** @type {number} */
    house_value;
    
    /** @type {Array} */
    rent = [];

    /** @type {number} */
    house_count = 0;

    /** @type {boolean} */
    mortgaged = false;

    /** @type {number} */
    mortgage_value;

    /** @type {Player} */
    owner;

    /** @type {number} */
    group = -1;

    constructor() {
        super();
    }

    /**
     * @returns {Property}
     */
    copy() {
        let property = new Property();
        this.copy_metadata(property);
        return property;
    }

    /**
     * 
     * @param {Game} game 
     * @param {Turn} [turn] 
     * @returns {number}
     */
    get_rent(game, turn) {
        if(!this.owner) {
            return 0;
        }
        switch(this.property_type) {
            case PropertyType.STREET:
            {
                if(this.mortgaged) {
                    return 0;
                }
                if(this.house_count > 0) {
                    return this.rent[this.house_count];
                }

                let streets = game.board.get_street_group(this.group);
                for(const street of streets) {
                    if(this.owner !== street.owner) {
                        return this.rent[0];
                    }
                }

                return this.rent[0]*2;
            }
            case PropertyType.RAILROAD_STATION:
            {
                let n = game.board.get_railroads().filter((railroad) => railroad.owner === this.owner).length;
                const values = [0, 25, 50, 100, 200];
                return values[n];
            }
            case PropertyType.UTILITY:
            {
                let n = game.board.get_utilities().filter((utility) => utility.owner === this.owner).length;
                if(turn) {
                    return turn.roll_result * (n > 1 ? 10 : 4);
                }
                else {
                    return n > 1 ? 10 : 4;
                }
            }
            default:
                return 0;
        }
    }

    /**
     * @param {Property} property
     */
    copy_metadata(property) {
        super.copy_metadata(property);
        property.property_type = this.property_type;
        property.value = this.value;
        property.house_value = this.house_value;
        property.rent = this.rent;
        property.mortgage_value = this.mortgage_value;
        property.group = this.group;
    }

    /**
     * @virtual
     * @param {Game} game
     */
    get_display_data(game) {
        let display_data = {
            name: this.name,    
            description: this.description,
            property: {
                property_type: this.property_type,
                value: this.value,
                house_value: this.house_value,
                rent: this.rent,
                house_count: this.house_count,
                mortgage_value: this.mortgage_value,
                mortgaged: this.mortgaged,
                group: this.group,
                cur_rent: this.get_rent(game)
            }
        };
        if (this.owner) {
            display_data.property.owner_id = this.owner.get_id();
        }
        return display_data;
    }

}