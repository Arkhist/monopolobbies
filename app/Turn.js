import { PlayerStatus } from './Player.js';

/** @typedef {import('./Game.js').default} Game */
/** @typedef {import('./Player.js').default} Player */
/** @typedef {import('./Space.js').default} Space */
/** @typedef {import('./Property.js').default} Property */

const AUCTION_SPEED = 10;

export const TurnState = {
    END: -2,                // 0

    JAIL_DECISION: -1,      // 1
    ROLL: 0,                // 1
    MOVE: 1,                // 0

    TAX: 2,                 // 1
    
    BUY_NEW: 3,             // 1
    AUCTION: 4,             // 0
    BANKRUPT_AUCTION: 7,    // 0

    FORCE_SELL: 5,          // 1

    MANAGE_PROPERTIES: 6,   // 1
};

export const TaxType = {
    RENT: 0,
    SIMPLE: 1,
};

export default class Turn {

    /** @type {number} */
    step;

    /** @type {Player} */
    player;

    /** @type {Game} */
    game;

    /** @type {number} */
    roll_result = 0;

    /** @type {boolean} */
    should_roll = false;

    /** @type {number} */
    double_amount = 0;

    /** @type {NodeJS.Timeout} */
    timeout;

    /** @type {{type: number, dest_id: number, amount: number}} */
    cur_tax = { type: undefined, dest_id: undefined, amount: undefined };

    /** @type {{property: Property, from_id: number, price: number, highest_bidder: number}} */
    cur_buy = { property: undefined, from_id: undefined, price: undefined, highest_bidder: undefined };

    /** @type {number} */
    owe = 0;
    
    /** @type {number} */
    auc_timestamp = null;
    auc_duration = 60;

    /** @type {Property[]} */
    bankrupcy_auctions = [];

    /**
     * @param {Game} game
     * @param {Player} player
     */
    constructor(game, player) {
        this.player = player;
        this.game = game;
    }


    
    get_display_data() {
        let data = {
            player_id: this.player.get_id(),
            step: this.step
        };
        if(this.step === TurnState.TAX) {
            data.cur_tax = this.cur_tax;
        }
        else if(this.step === TurnState.BUY_NEW) {
            data.cur_buy = {
                location: this.cur_buy.property.location,
                price: this.cur_buy.price
            };
        }
        else if(this.step === TurnState.AUCTION || this.step === TurnState.BANKRUPT_AUCTION) {
            data.cur_buy = {
                location: this.cur_buy.property.location,
                price: this.cur_buy.price,
                bidder: this.cur_buy.highest_bidder,
                time_left: this.auc_duration * 1000 - (Date.now() - this.auc_timestamp)
            };
        }
        else if(this.step === TurnState.FORCE_SELL) {
            data.owe = this.owe;
        }
        return data;
    }



// ==================== //
// TURN LOGIC HANDLING  //
// ==================== //


    /**
     * @Notifies
     * Start the turn and initiates the turn automaton
     */
    start() {
        if(this.player.status === PlayerStatus.BANKRUPT) {
            this.do_bankrupt();
        }
        else if(this.player.status === PlayerStatus.OUT) {
            this.end();
        }
        else if(this.player.money < 0) {
            this.step = TurnState.FORCE_SELL;
            this.owe = this.get_owe();
        }
        else if(this.player.is_jailed()) {
            this.step = TurnState.JAIL_DECISION;
        }
        else {
            this.step = TurnState.ROLL;
        }
        this.game.notify_turn(true);
    }

    /**
     * @Notifies
     * Ends turn
     */
    end() {
        if(this.step === TurnState.END) {
            return;
        }

        this.step = TurnState.END;
        this.timeout = setTimeout(() => { this.game.end_turn(); }, 0.25*1000);

        this.game.notify_turn(false);
    }
    
    /**
     * @Notifies
     * Ends the current roll round
     */
    on_round_end() {
        if(this.should_roll) {
            this.step = TurnState.ROLL;
        }
        else {
            this.step = TurnState.MANAGE_PROPERTIES;
        }
        if(this.player.money < 0) {
            this.step = TurnState.FORCE_SELL;
        }
        this.game.notify_turn(false);
    }



// ==================== //
//  MOVEMENT HANDLING   //
// ==================== //

    /**
     * @Notifies
     * Roll then move, escape jail or go to jail
     */
    roll() {
        if(this.step !== TurnState.ROLL && this.step !== TurnState.JAIL_DECISION) {
            return;
        }
        this.step = TurnState.MOVE;

        clearTimeout(this.timeout);

        let d1 = Math.floor(Math.random() * 6 + 1);
        let d2 = Math.floor(Math.random() * 6 + 1);

        this.roll_result = d1+d2;

        if(d1 === d2) {
            this.double_amount++;
            this.should_roll = true;
            if(this.player.is_jailed()) {
                this.should_roll = false;
                this.game.jail(this.player, true);
            }
        }
        else {
            this.should_roll = false;
        }

        if(this.double_amount >= 3 || this.player.is_jailed()) {
            this.game.jail(this.player);
            this.step = TurnState.MANAGE_PROPERTIES;
        }
        else {
            this.timeout = setTimeout(() => {
                this.game.advance_player_to(this.player, this.player.position + this.roll_result);
            }, 0.25*1000);
        }
        this.player.emit('game', {type:'action', action:'roll-result', data:{d1: d1, d2: d2, d:this.double_amount}});
        this.game.send_message(this.player.get_name() + ' rolled '+d1+' and '+d2);
        this.game.notify_turn(false);
    }


    /**
     * Handles what happens when the player moves to a space
     * @param {Space} space 
     * @param {boolean} [is_handled]
     */
    handle_space(space, is_handled) {
        if(is_handled) {
            this.on_round_end();
        }
        else {
            this.timeout = setTimeout(() => {
                if(space.constructor.name === 'Property') {
                    this.handle_property(/** @type {Property} */ (space));
                }
                else {
                    space.on_enter(this, space, this.player);
                }
            }, 0.25*1000);
        }
    }

    /**
     * @Notifies
     * Handles what happens when a player lands on a property
     * @param {Property} property 
     */
    handle_property(property) {
        if (property.owner) { // Tax routine
            if (property.mortgaged || property.owner === this.player) {
                this.on_round_end();
                return;
            }
            else {
                this.cur_tax.type = TaxType.RENT;
                this.cur_tax.dest_id = property.owner.get_id();
                this.cur_tax.amount = property.get_rent(this.game, this);
                if(this.cur_tax.amount > this.player.money) {
                    this.resolve_bankrupcy();
                    return;
                }
                else {
                    this.step = TurnState.TAX;
                }
            }
        }
        else { // Buy routine
            this.cur_buy.price = property.value;
            this.cur_buy.property = property;
            this.step = TurnState.BUY_NEW;
        }
        this.game.notify_turn(false);
    }



// ==================== //
//  BANKRUPCY HANDLING  //
// ==================== //

    /**
     * Get amount of money that the player currently owes
     */
    get_owe() {
        let base = this.cur_tax.amount ? this.cur_tax.amount : 0;
        return base;
    }

    /**
     * @Notifies
     * Starts bankrupt logic for the player and outs them from the game
     */
    do_bankrupt() {
        this.player.status = PlayerStatus.OUT;
        this.game.broadcast({type:'player', id:this.player.get_id(), status: 2});

        let owned = this.game.board.get_properties_of(this.player);
        let total_money = Math.max(0, this.player.money);
        for(const property of owned) {
            if(property.house_count > 0) {
                total_money += property.house_count * property.house_value/2;
                this.game.set_real_estate(property, 0, true);
            }
            this.bankrupcy_auctions.push(property);
        }

        this.game.transfer_money(this.player, undefined, this.player.money);
        this.game.transfer_money(undefined, this.game.get_player_from_id(this.cur_tax.dest_id), total_money);

        if(this.bankrupcy_auctions.length > 0) {
            this.step = TurnState.BANKRUPT_AUCTION;

            let property = this.bankrupcy_auctions.pop();
            if(property) {
                this.cur_buy.price = property.mortgaged ? property.mortgage_value * 0.1 : 0;
                this.cur_buy.property = property;
                this.cur_buy.highest_bidder = undefined;
    
                this.auc_duration = AUCTION_SPEED*2;
                this.auc_timestamp = Date.now();
    
                this.timeout = setTimeout(() => {
                    this.resolve_auction();
                }, AUCTION_SPEED*2*1000);
            }

            this.game.notify_turn(false);
        }
        else {
            this.end();
        }
    }

    /**
     * @Notifies
     * Force player to get what he owes before continuing
     */
    resolve_bankrupcy() {
        this.owe = this.get_owe();
        this.step = TurnState.FORCE_SELL;
        this.game.notify_turn(false);
    }



// ==================== //
//   AUCTION HANDLING   //
// ==================== //


    /**
     * Updates the current bid and resets the auction timer
     * @param {Player} player 
     * @param {number} amount 
     */
    update_auction(player, amount) {
        clearTimeout(this.timeout);

        this.cur_buy.price = amount;
        this.cur_buy.highest_bidder = player.get_id();

        this.auc_duration = AUCTION_SPEED;
        this.auc_timestamp = Date.now();

        this.timeout = setTimeout(() => {
            this.resolve_auction();
        }, AUCTION_SPEED*1000);

        this.game.broadcast({type: 'auction', data:{
            location: this.cur_buy.property.location,
            price: this.cur_buy.price,
            bidder: this.cur_buy.highest_bidder,
            time_left: this.auc_duration * 1000 - (Date.now() - this.auc_timestamp)
        }});
    }

    /**
     * Completes the current auction and if in a bankrupt auction, sets up the next auction
     */
    resolve_auction() {
        clearTimeout(this.timeout);
        let winner = this.game.get_player_from_id(this.cur_buy.highest_bidder)
        if(winner === undefined) {
            let active = this.game.players.filter((player) => player.status === PlayerStatus.ACTIVE);
            if(active.length > 0) {
                winner = active[Math.floor(Math.random() * active.length)];
            }
            else {
                winner = undefined;
            }
        }
        if(!winner) {
            this.end();
            return;
        }
        this.game.grant_property(this.cur_buy.property, this.game.get_player_from_id(this.cur_buy.from_id), winner, this.cur_buy.price);

        this.game.send_message(winner.get_name() + ' won the auction of ' + this.cur_buy.property.name + ' for $' + this.cur_buy.price);

        if(this.step === TurnState.AUCTION) {
            this.on_round_end();
        }
        else if(this.step === TurnState.BANKRUPT_AUCTION) {
            let property = this.bankrupcy_auctions.pop();
            if(property) {
                this.cur_buy.price = property.mortgaged ? property.mortgage_value * 0.1 : 0;
                this.cur_buy.property = property;
                this.cur_buy.highest_bidder = undefined;
    
                this.auc_duration = AUCTION_SPEED*2;
                this.auc_timestamp = Date.now();
    
                this.timeout = setTimeout(() => {
                    this.resolve_auction();
                }, AUCTION_SPEED*2*1000);

                this.game.notify_turn(false);
            }
            else {
                //this.game.end_player(this.player);
                this.end();
            }
        }
    }



// ==================== //
//   ACTION HANDLING    //
// ==================== //

    /**
     * Handles the received action from player
     * @param {Player} player 
     * @param {*} action 
     */
    handle_action(player, action) {
        if(player.status !== PlayerStatus.ACTIVE) {
            return;
        }

        if (this.player === player) {
            if(action.type === 'roll' && this.step === TurnState.ROLL) {
                this.roll();
            }
            else if(action.type === 'pay_tax' && this.step === TurnState.TAX) {
                let dest_id = undefined;
                if(this.cur_tax.type === TaxType.RENT) {
                    dest_id = this.cur_tax.dest_id;
                }
                if(this.player.money >= this.cur_tax.amount) {
                    this.game.transfer_money(this.player, this.game.get_player_from_id(dest_id), this.cur_tax.amount);
                    this.on_round_end();
                    this.cur_tax = { type: undefined, dest_id: undefined, amount: undefined };
                }

            }
            else if(action.type === 'buy_property' && this.step === TurnState.BUY_NEW) {
                if(action.buy_type === 'buy') {
                    if(this.player.money >= this.cur_buy.price) {
                        this.game.grant_property(this.cur_buy.property, undefined, this.player, this.cur_buy.price);
    
                        this.on_round_end();
                    }
                }
                else if(action.buy_type === 'auction') {
                    this.step = TurnState.AUCTION;

                    this.cur_buy.price = 0;
                    this.cur_buy.highest_bidder = undefined;

                    this.auc_duration = AUCTION_SPEED*2;
                    this.auc_timestamp = Date.now();

                    this.timeout = setTimeout(() => {
                        this.resolve_auction();
                    }, AUCTION_SPEED*2*1000);
                    this.game.notify_turn(false);
                }
            }
            else if(action.type === 'continue') {
                if(this.step === TurnState.MANAGE_PROPERTIES) {
                    this.end();
                }
                else if(this.step === TurnState.FORCE_SELL) {
                    if(this.player.money >= this.owe) {
                        if(this.cur_tax.amount) {
                            this.step = TurnState.TAX;
                            this.game.notify_turn(false);
                        }
                        else if(this.player.is_jailed()) {
                            this.step = TurnState.JAIL_DECISION;
                            this.game.notify_turn(false);
                        }
                        else {
                            this.on_round_end();
                        }
                    }
                }
            }
            else if(action.type === 'jail_escape' && this.step === TurnState.JAIL_DECISION) {
                if(action.choice === 'roll') {
                    this.roll();
                }
                else if(action.choice === 'fine') {
                    if(this.player.money < 50) {
                        return;
                    }
                    this.game.transfer_money(this.player, undefined, 50);
                    this.step = TurnState.ROLL;
                    this.game.jail(player, true);
                    this.game.notify_turn(false);
                }
                else if(action.choice === 'card') {
                    if(player.can_get_out_of_jail()) {
                        player.use_get_out_of_jail();
                        this.step = TurnState.ROLL;
                        this.game.jail(player, true);
                        this.game.notify_turn(false);
                    }
                }
            }
        }
        if(this.step === TurnState.AUCTION || this.step === TurnState.BANKRUPT_AUCTION) {
            if(action.type === 'outbid' && action.amount && typeof action.amount === 'number') {
                if(player.get_id() !== this.cur_buy.highest_bidder) {
                    if(player.money >= action.amount && action.amount > this.cur_buy.price) {
                        this.update_auction(player, action.amount);
                    }
                }
            }
        }
        else if(action.type === 'mortgage_property' && action.location !== undefined && typeof action.location === 'number' && action.location >= 0 && action.location < 40) {
            this.game.mortgage_property(player, action.location, true);
        }
        else if(action.type === 'unmortgage_property' && action.location !== undefined && typeof action.location === 'number' && action.location >= 0 && action.location < 40) {
            this.game.mortgage_property(player, action.location, false);
        }
        else if(action.type === 'change_property_houses' && action.location !== undefined && typeof action.location === 'number' && action.location >= 0 && action.location < 40) {
            if(action.amount !== undefined && typeof action.amount === 'number' && action.amount >= 0 && action.amount <= 5) {
                let property = /** @type {Property} */ (this.game.board.get(action.location));
                if(property.owner !== player) {
                    return;
                }
                this.game.set_real_estate(property, action.amount);
            }
        }
        else if(action.type === 'declare_bankrupt') {
            this.game.set_bankrupt(player);
        }
        else if(action.type === 'trade') {
            if(action.action === 'propose') {
                if(!action.trade) {
                    return;
                }
                let trade = action.trade;
                if(!trade.self || !trade.self.properties) {
                    return;
                }
                if(!trade.other || !trade.other.properties || typeof trade.other.id !== 'number') {
                    return;
                }
                if(typeof trade.money !== 'number') {
                    return;
                }

                let other = this.game.get_player_from_id(trade.other.id);
                if(!other || other === player || other.status !== PlayerStatus.ACTIVE) {
                    return;
                }

                if(trade.money > 0 ? player.money < trade.money : other.money < -1 * trade.money) {
                    return;
                }

                for(const pId of trade.self.properties) {
                    let property = /** @type {Property} */ (this.game.board.get(pId));
                    if(property.owner !== player) {
                        return;
                    }
                    for(const prop of this.game.board.get_street_group(property.group)) {
                        if(prop.house_count > 0) {
                            return;
                        }
                    }
                }
                for(const pId of trade.other.properties) {
                    let property = /** @type {Property} */ (this.game.board.get(pId));
                    if(property.owner !== other) {
                        return;
                    }
                    for(const prop of this.game.board.get_street_group(property.group)) {
                        if(prop.house_count > 0) {
                            return;
                        }
                    }
                }
                if(other.current_trade) {
                    return;
                }

                if(!player.current_trade || other.get_id() === player.current_trade.send.id) {
                    player.send_trade(trade);
                    other.send_trade(trade, player);
                }
            }
            else if(player.current_trade && player.current_trade.send.id !== player.get_id()) {
                let other = this.game.get_player_from_id(player.current_trade.send.id);
                if (action.action === 'accept') {
                    this.game.apply_trade(player, other);
                }
                else if (action.action === 'reject') {
                    player.clear_trade();
                    other.clear_trade();
                }
            }
        }
    }

}