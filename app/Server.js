import Client from "./Client.js"
import Lobby from "./Lobby.js"
import { set_bad_request_response } from "./utils/http-helper.js";
import Chat, {ChatChannel} from "./Chat.js";

/** 
 * @typedef {import('socket.io').Server} SocketServer 
 * @typedef {import('socket.io').Socket} SocketClient
 * @typedef {import('express').Express} Express
 * @typedef {import('http').Server} Http
 * @typedef {import('express').Response} ExpResponse
 * @typedef {import('./stages/HomeStage.js').default} HomeStage
 */

export default class Server extends Chat {

    /** @private @type {Express} */
    expApp = undefined;
    /** @private @type {SocketServer} */
    io = undefined;
    /** @private @type {Http} */
    http = undefined;

    /** @private @type {Client[]} */
    clients = [];
    
    local_mode = true;
    
    last_client_id = 0;

    /**
     * @param {Express} expApp 
     * @param {SocketServer} io 
     * @param {Http} http 
     */
    constructor(expApp, io, http) {
        super();
        this.expApp = expApp;
        this.io = io;
        this.http = http;

        this.setup_routing();
    }

    start_server() {
        this.http.listen(3000, function () {
            console.log('Listening on *:3000');
        });
        
        this.io.on('connection', (socket) => {
            let client = this.connect_socket(socket);
            socket.on('disconnect', () => {
                this.disconnect_socket(client, socket);
            });
        });
    }

    /**
     * 
     * @param {string} ssnid
     * @returns {?Client} 
     */
    get_client_from_ssnid(ssnid) {
        return this.clients.find((val) => val.session.id === ssnid);
    }


    /**
     * @param {SocketClient} socket
     */
    connect_socket(socket) {
        let ssn = socket.request.session;
        let client = this.get_client_from_ssnid(ssn.id);
        if (client === undefined) {
            client = new Client(this, ssn, this.last_client_id);
            this.last_client_id++;
            this.clients.push(client);
        }
        client.connect_socket(socket);
        return client;
    }

    /**
     * Disconnects socket from server.
     * @param {Client} client
     * @param {SocketClient} socket 
     */
    disconnect_socket(client, socket) {
        let found = this.clients.find((val, id, arr) => {
            if(val === client) 
                return true;
        });
        if(found === undefined) 
            return;
        found.disconnect_socket(socket);
    }

    /**
     * @param {Client} client
     */
    disconnect_client(client) {
        this.clients = this.clients.filter((other) => other !== client);
    }

    /**
     * If the user is authenticated, redirect to the right url.
     * @param {string} url
     * @param {any} req
     * @param {any} res
     */
    redirect_client_middleware(url, req, res) {
        if (req.method === 'GET') {
            let client = this.get_client_from_ssnid(req.session.id);
            if(client) {
                return client.check_url_and_redirect(url, res);
            }
            else if(url !== '/') {
                res.redirect('/');
                res.end();
                return true;
            } 
        }
        return false;
    }

    setup_routing() {
        this.expApp.use((req, res, next) => {
            if (this.redirect_client_middleware(req.url, req, res)) {
                return;
            }
            next();
        });

        this.expApp.get('/', (req, res) => {
            this.send_page(res, 'index');
        });

        this.expApp.get('/home', (req, res) => {
            this.send_page(res, 'home');
        });

        this.expApp.get('/lobby/*', (req, res) => {
            this.send_page(res, 'lobby');
        });

        this.expApp.get('/game/*', (req, res) => {
            this.send_page(res, 'game');
        });

        this.expApp.post('/request', (req, res) => {
            let client = this.get_client_from_ssnid(req.session.id);
            if(client !== undefined) {
                if (!client.handle_request(req.body, res)) {
                    set_bad_request_response(res);
                }
            }
            else {
                res.statusCode = 401; // Unauthenticated
                res.statusMessage = "Unauthenticated: this endpoint is only available when in a lobby or a game.";
                res.end();
            }
        });
    }

    /**
     * Sends page "partial" to the response, using additionalData for rendering.
     * @param {ExpResponse} response 
     * @param {String} partialName 
     * @param {*} [additionalData] 
     */
    send_page(response, partialName, additionalData) {
        this.expApp.render(partialName + '.ejs', {
            data: additionalData
        }, (err, html) => {
            response.render('layout', {
                content: err != null ? err : html,
                data: additionalData,
                isLocal: this.local_mode
            });
        });
    }

    //----- CHAT SYSTEM -----//

    /** @private */
    globalChatChannel = new ChatChannel("global", "Global");

    /**
     * @param {string} channel
     */
    get_channel(channel) {
        if(channel === 'global') {
            return this.globalChatChannel;
        }
        if(channel.startsWith('lobby-')) {
            channel = channel.slice(6);
            let id = parseInt(channel);
            let lobby = this.lobbies.find((val) => val.id == id);
            if(!lobby)
                return undefined;
            return lobby.get_channel();
        }
    }

    get_lobbies() {
        return this.lobbies;
    }

    //----- LOBBY SYSTEM -----//

    /** @private @type {Lobby[]} */
    lobbies = [];

    last_lobby_id = 0;

    /** @type {HomeStage[]} */
    home_listeners = [];

    /**
     * @param {Client} owner
     * @returns {Lobby}
     */
    create_lobby(owner) {
        let lobby = new Lobby(this.last_lobby_id, this, owner);
        this.last_lobby_id++;

        lobby.room_config.name = owner.username+'\'s Room';
        this.lobbies.push(lobby);
        this.notify_update_lobby(lobby);
        return lobby;
    }

    /**
     * @param {Lobby} lobby
     */
    notify_update_lobby(lobby) {
        this.home_listeners.forEach((listener) => {
            listener.on_lobby_update(lobby.get_display_data());
        });
    }

    /**
     * @param {Lobby} lobby
     */
    remove_lobby(lobby) {
        lobby.close();
        this.lobbies = this.lobbies.filter((val) => val !== lobby);

        this.home_listeners.forEach((listener) => {
            listener.on_lobby_update({id:lobby.id});
        });
    }

    /**
     * @param {HomeStage} listener
     */
    add_home_listener(listener) {
        this.home_listeners.push(listener);
    }

    /**
     * @param {HomeStage} listener
     */
    remove_home_listener(listener) {
        this.home_listeners = this.home_listeners.filter((val) => val !== listener);
    }
}