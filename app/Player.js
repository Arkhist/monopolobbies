/** @typedef {import('./Property.js').default} Property */

import Client from "./Client.js";
import Game from "./Game.js";

export const PlayerStatus = {
    ACTIVE: 0,
    BANKRUPT: 1,
    OUT: 2
}

const NULL_TRADE = () => {
    return {money:null, send:{id:null, properties:[]}, recv:{id:null, properties:[]}};
};

export default class Player {
    
    /** @type {Client} */
    client;

    /** @type {string} */
    name;

    /** @type {Game} */
    game;

    position = 0;

    money = 1500;
    
    jailed = false;

    get_out_of_jail = false;

    status = PlayerStatus.ACTIVE;

    /** @type {{money: number, send:{id:number, properties:number[]}, recv:{id:number, properties:number[]}}} */
    current_trade = null;

    /**
     * @param {Game} game
     * @param {Client} client
     */
    constructor(game, client) {
        this.game = game;
        this.client = client;
        this.name = this.client.username;
    }

    is_jailed() {
        return this.jailed;
    }

    get_id() {
        return this.client.id;
    }

    get_name() {
        return this.name;
    }

    use_get_out_of_jail() {
        this.get_out_of_jail = false;
    }

    /**
     * @returns {boolean}
     */
    can_get_out_of_jail() {
        return this.get_out_of_jail;
    }

    get_display_data() {
        return {
            id: this.get_id(),
            money: this.money,
            position: this.position,
            name: this.get_name(),
            jailed: this.is_jailed(),
            status: this.status
        };
    }

    /**
     * 
     * @param {*} trade 
     * @param {Player} [sender] 
     */
    send_trade(trade, sender) {
        let current_trade = NULL_TRADE();
        
        current_trade.send.id = sender ? sender.get_id() : this.get_id();
        current_trade.recv.id = !sender ? trade.other.id : this.get_id();
        current_trade.money = trade.money;
        current_trade.send.properties = sender ? trade.self.properties : trade.other.properties;
        current_trade.recv.properties = !sender ? trade.self.properties : trade.other.properties;


        this.current_trade = current_trade;

        this.emit('game', {type:'trade', trade: this.current_trade});
    }

    clear_trade() {
        if(!this.current_trade) {
            return;
        }
        this.current_trade = null;
        this.emit('game', {type:'trade', trade: null});
    }

    /**
     * 
     * @param {string} event 
     * @param {*} msg 
     */
    emit(event, msg) {
        if(this.client) {
            this.client.emit(event, msg);
        }
    }
}