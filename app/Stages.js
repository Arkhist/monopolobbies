import RegisterStage from './stages/RegisterStage.js';
import HomeStage from './stages/HomeStage.js';
import LobbyStage from './stages/LobbyStage.js';
import GameStage from './stages/GameStage.js';
import ChatStage from './stages/ChatStage.js';


const Stages = {
    REGISTER: RegisterStage,
    HOME: HomeStage,
    LOBBY: LobbyStage,
    GAME: GameStage,
    CHAT: ChatStage
};

export default Stages;