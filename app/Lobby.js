import Game from './Game.js'
import LobbyStage from './stages/LobbyStage.js';
import ChatChannelHolder, { ChatChannel } from './Chat.js';

/** @typedef {import('./Client.js').default} Client */
/** @typedef {import('./Server.js').default} Server */

export default class Lobby extends ChatChannelHolder {
    
    /** @type {Game} */
    game;

    /** @type {Number} */
    id;

    /** @type {Client} */
    owner;

    /** @type {Client[]} */
    clients = [];

    /**
     * @type {ChatChannel}
     */
    channel;

    /**
     * @type {boolean}
     */
    running = false;

    room_config = {
        name: 'Default Room',
        password: '',
        gamemode: 'classic'
    }

    /**
     * @param {number} id
     * @param {Server} server
     * @param {Client} owner
     */
    constructor(id, server, owner) {
        super()
        this.id = id;
        this.server = server;
        this.owner = owner;
        this.channel = new ChatChannel('lobby-' + this.id, "Local");
    }

    start_game() {
        if(this.clients.length < 2) {
            return;
        }
        let game = new Game(this);
        let r = game.load();
        if(r.ok) {
            this.game = game;
            this.running = true;
            this.clients.forEach((client) => {
                (/** @type {LobbyStage} */ (client.current_stage)).game_start(game, 
                    game.get_player_from_client(client));
            });
            this.lobby_updated();
            
            this.send_message('System', 'Starting a new game.');
            game.start();
        }
        this.clients.forEach((client) => {
            client.emit('lobby', {type:'start', data:r});
        });
    }

    game_complete() {
        if(this.game) {
            for(const client of this.clients) {
                /** @type {LobbyStage} */ (client.current_stage).game_over();
            }
            this.running = false;
            this.game = null;
        }
    }

    /**
     * @param {Client} client
     */
    add_client(client) {
        this.clients.push(client);
        this.lobby_updated();
    }

    /**
     * @param {Client} client
     */
    remove_client(client) {
        this.clients = this.clients.filter((val) => val !== client);
        if(this.clients.length == 0) {
            this.server.remove_lobby(this);
            return;
        }
        if(client === this.owner) {
            this.owner = this.clients[0];
        }
        this.lobby_updated();
    }

    lobby_updated() {
        let update_data = this.get_update_data();

        this.clients.forEach((client) => {
            if(client.current_stage.event_name !== 'lobby') {
                return;
            }

            (/** @type {LobbyStage} */ (client.current_stage)).on_lobby_update(update_data);
        })
        this.server.notify_update_lobby(this);
    }

    /**
     * Broadcasts event to all clients in lobby
     * @param {String} event 
     * @param {Object} msg 
     */
    broadcast(event, msg) {
        this.clients.forEach((client) => {
            client.emit(event, msg);
        });
    }

    /**
     * Send event to one client
     * @param {Number} id 
     * @param {String} event 
     * @param {Object} msg 
     */
    send(id, event, msg) {
        if(this.clients[id])
            this.clients[id].emit(event, msg);
    }

    get_display_data() {
        return {
            id: this.id,
            name: this.room_config.name,
            gamemode: this.room_config.gamemode,
            locked: this.room_config.password.length > 0,
            players: this.clients.length,
            running: this.running
        };
    }

    get_update_data() {
        let player_list = [];
        this.clients.forEach((client) => {
            player_list.push({name:client.username, owner: client === this.owner, id: client.id});
        });

        return {
            id: this.id,
            players: player_list,
            gamemode: this.room_config.gamemode,
            name: this.room_config.name
        };
    }

    /**
     * @param {string} [name]
     */
    get_channel(name) {
        return this.channel;
    }

    /**
     * @param {string} author
     * @param {string} msg
     */
    send_message(author, msg) {
        this.channel.add_message({author: author, message: msg});
    }

    close() {
        this.game_complete();
    }
}