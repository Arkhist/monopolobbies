/** @typedef {import('./Player.js').default} Player */
/** @typedef {import('./Game.js').default} Game */

import fs from 'fs'

import Space from './Space.js'
import Property, { PropertyType } from './Property.js'

import Deck, {get_deck} from './Deck.js'
import {get_space} from './SpaceLoader.js'

import { dirname } from 'path';
import { fileURLToPath } from 'url';


// @ts-ignore
const DIRNAME = dirname(fileURLToPath(import.meta.url));

let board_stored = {};

/**
 * 
 * @param {string} name Name of the board
 * @returns {Board} Generated board
 */
function generate_board(name){
    let board_data;

    // Load board data
    try {
        let boardfd = fs.openSync(DIRNAME + '/boards/' + name + '/board.json', 'r');
        if (boardfd == -1) {
            console.log("Error while generating board " +name+": /boards/"+name+"/board.json not opened");
            return undefined;
        }
        let board_content = fs.readFileSync(boardfd);
        fs.closeSync(boardfd);
        board_data = JSON.parse(board_content.toString());
    }
    catch (err) {
        console.log("Error while generating board " +name+": "+err);
        return undefined;
    }

    
    let board = new Board();
    if(board_data.name)
        board.name = board_data.name;
    if(board_data.decks) {
        for(let deck_name in board_data.decks) {
            board.decks[deck_name] = get_deck(board_data.decks[deck_name]);
            if(!board.decks[deck_name]) {
                return undefined;
            }
        }
    }
    if(board_data.spaces) {
        for(let i = 0;i < board_data.spaces.length; i++) {
            board.spaces[i] = get_space(name,board_data.spaces[i]);
            board.spaces[i].location = i;
            if(!board.spaces[i]) {
                return undefined;
            }
        }
    }
    
    board_stored[name] = board;
    return board;
}

/**
 * 
 * @param {string} name 
 */
export function get_board(name) {
    if(!board_stored[name])
        generate_board(name);
    if(!board_stored[name]) {
        return undefined;
    }
    return board_stored[name].copy();
}

export default class Board {
    /** @type {string} */
    name;
    
    /** @type {Object.<string, Deck>} */
    decks = {};
    
    /** @type {Space[]} */
    spaces = [];
    
    go_money = 200;
    
    /**
     * @return {Board}
     */
    copy() {
        let board = new Board();
        board.name = this.name;
        for(let deck_name in this.decks) {
            board.decks[deck_name] = this.decks[deck_name].copy();
        }
        for(let i = 0; i < this.spaces.length; i++) {
            board.spaces[i] = this.spaces[i].copy();
        }
        return board;
    }
    
    /**
     * @param {number} location
     * @returns {Space}
     */
    get(location) {
        return this.spaces[location];
    }


    /**
     * 
     * @param {number} group
     * @returns {Property[]}
     */
    get_street_group(group) {
        // @ts-ignore
        return this.spaces.filter((space) => { return (space.property_type !== undefined && space.group === group); });
    }
    
    /**
     * @returns {Property[]}
     */
    get_railroads() {
        
        let railroads = this.spaces.filter((space) => {
            // @ts-ignore
            return (space.property_type !== undefined && space.property_type === PropertyType.RAILROAD_STATION); });
        // @ts-ignore
        return railroads;
    }

    /**
     * @returns {Property[]}
     */
    get_utilities() {
        // @ts-ignore
        return this.spaces.filter((space) => { return (space.property_type !== undefined && space.property_type === PropertyType.UTILITY); });
    }

    /**
     * @param {Player} player
     * @returns {Property[]}
     */
    get_properties_of(player) {
        // @ts-ignore
        return this.spaces.filter((space) => { 
            // @ts-ignore
            return (space.property_type !== undefined && space.owner === player); });
    }

    /**
     * 
     * @param {Game} game 
     */
    get_display_data(game) {
        let display_data = {
            name: this.name,
            spaces: []
        };
        this.spaces.forEach((space) => {
            display_data.spaces.push(space.get_display_data(game));
        });

        return display_data;
    }
}