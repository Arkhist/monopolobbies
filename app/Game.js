import Player, { PlayerStatus } from './Player.js';
import { get_board } from './Board.js';
import Turn from './Turn.js';

/** @typedef {import('./Lobby.js').default} Lobby */
/** @typedef {import('./stages/LobbyStage.js').default} LobbyStage */
/** @typedef {import('./Board.js').default} Board */
/** @typedef {import('./Client.js').default} Client */
/** @typedef {import('./Property.js').default} Property */
/** @typedef {SocketIO.Socket} ClientSocket */

export default class Game {

    /** @type {Lobby} */
    lobby = undefined;

    /** @type {Board} */
    board = undefined;
    
    /** @type {Player[]} */
    players = [];

    bank_money = 0;

    /** @type {Turn} */
    current_turn;

    turn_index = 0;

    available_houses = 32;
    available_hotels = 12;
    

    /**
     * @param {Lobby} lobby
     */
    constructor(lobby) {
        this.lobby = lobby;
    }

    /**
     * @param {boolean} is_new
     * @param {ClientSocket} [socket]
     */
    notify_turn(is_new, socket) {
        if(this.current_turn) {
            if(socket) {
                socket.emit('game', {type:'turn', new:is_new, data:this.current_turn.get_display_data()});
            }
            else {
                this.players.forEach((player) => {
                    player.emit('game', {type:'turn', new:is_new, data:this.current_turn.get_display_data()});
                });
            }
        }
    }

    start() {
        for(const deckname in this.board.decks) {
            this.board.decks[deckname].shuffle();
        }
        this.end_turn();
    }

    /**
     * @returns {{ok:boolean, err?:string}}
     */
    load() {
        this.board = get_board(this.lobby.room_config.gamemode);
        /*if(!this.board) {
            return {ok:false, err:'Unable to load board from gamemode: '+this.lobby.room_config.gamemode};
        }*/
        for(const client of this.lobby.clients) {
            this.players.push(new Player(this, client));
        }
        return {ok:true};
    }

    /**
     * 
     * @param {Player} player 
     */
    set_bankrupt(player) {
        player.status = PlayerStatus.BANKRUPT;
        this.broadcast({type:'player', id:player.get_id(), status: 1});
        if(this.current_turn && this.current_turn.player === player) {
            this.current_turn.do_bankrupt();
        }
    }
    

    end_turn() {
        if(this.players.filter(player => player.status === PlayerStatus.ACTIVE).length <= 1) {
            this.game_over();
            this.current_turn = null;
        }
        else {
            while(this.players[this.turn_index % this.players.length].status === PlayerStatus.OUT) {
                this.turn_index++;
            }
    
            this.current_turn = new Turn(this, this.players[this.turn_index % this.players.length]);
            this.turn_index++;
            this.current_turn.start();
        }
    }

    game_over() {
        let winner = this.players.find(player => player.status === PlayerStatus.ACTIVE);
        this.send_message('The game is over ! The winner is... ' + (winner ? winner.get_name() + '!' : 'nobody...'));
        this.broadcast({type:'game-over'});
        this.lobby.game_complete();
    }

    /**
     * @param {Player} player 
     * @param {*} action 
     */
    handle_action(player, action) {
        if(this.current_turn) {
            this.current_turn.handle_action(player, action);
        }
    }

    /**
     * @param {Client} client
     * @returns {Player?}
     */
    get_player_from_client(client) {
        return this.players.find((val) => val.client === client);
    }

    /**
     * @param {number} [id]
     * @returns {Player?}
     */
    get_player_from_id(id) {
        return this.players.find((val) => val.get_id() === id);
    }

    get_players_display() {
        let players_display = [];
        this.players.forEach((player) => {
            players_display.push(player.get_display_data());
        });
        return players_display;
    }

    /**
     * 
     * @param {Player} player 
     * @returns {number}
     */
    get_asset_value(player) {
        let properties = this.board.get_properties_of(player);
        let value = 0;
        for(const property of properties) {
            if(property.owner === player) {
                value += property.mortgaged ? 0 : property.mortgage_value;
                value += property.house_count * property.house_value/2;
            }
        }
        return value;
    }



    /**
     * @param {Player} player
     * @param {number} location
     */
    advance_player_to(player, location) {
        location = location%40;
        if(player.position > location) { // Salary from start space
            this.transfer_money(undefined, player, this.board.go_money);
        }
        player.position = location;
        this.current_turn.handle_space(this.board.get(location));
        this.broadcast({type:'movement', from:player.get_id(), towards:location});
    }

    /**
     * @param {Player} player
     * @param {number} location
     * @param {boolean} [is_handled]
     */
    teleport(player, location, is_handled) {
        player.position = location;
        this.broadcast({type:'movement', from:player.get_id(), towards:location});
        this.current_turn.handle_space(this.board.get(location), is_handled);
    }
    
    /**
     * Ends current turn if related to play then jails player
     * @param {Player} player 
     * @param {boolean} [unjail]
     */
    jail(player, unjail) {
        if(!unjail && !player.is_jailed()) {
            if(player === this.current_turn.player) {
                this.current_turn.end();
            }
            player.jailed = true;
            this.teleport(player, 10);
            this.broadcast({type: 'player', id: player.get_id(), jailed: true});
        }
        else {
            this.broadcast({type: 'player', id: player.get_id(), jailed: false});
            player.jailed = false;
        }
    }

    /**
     * @param {Player} source
     * @param {Player} destination
     * @param {number} amount
     */
    transfer_money(source, destination, amount) {
        if(amount === 0 || amount == null || amount === undefined || isNaN(amount)) {
            return;
        }

        if(source) {
            source.money -= amount;
        }
        if(destination) {
            destination.money += amount;
        }
        else {
            this.bank_money += amount;
        }
        let srcId = (source) ? source.get_id() : -1;
        let destId = (destination) ? destination.get_id() : -1;
        this.broadcast({type:'transfer', from:srcId, to:destId, amount:amount});
    }

    /**
     * 
     * @param {Property} property 
     * @param {Player?} from
     * @param {Player} to 
     * @param {number} price 
     */
    grant_property(property, from, to, price) {
        property.owner = to;
        this.broadcast({type:'property', owner:to.get_id(), location:property.location});
        this.transfer_money(to, from, price);
    }

    /**
     * Changes the amount of houses of property to house_amount
     * @param {Property} property 
     * @param {number} house_amount 
     * @param {boolean} [bank_action] 
     */
    set_real_estate(property, house_amount, bank_action) {
        if(!bank_action) {
            if(property.house_count === house_amount || property.mortgaged) {
                return;
            }
    
            let group = this.board.get_street_group(property.group);
            for(const p of group) {
                if(p.owner !== property.owner || p.mortgaged) {
                    return;
                }
                if(Math.abs(p.house_count-house_amount) >= 2 && p !== property) {
                    return;
                }
            }
    
            if(property.house_count > house_amount) { // Removing houses
                let money = (property.house_count - house_amount) * property.house_value/2;
                this.transfer_money(undefined, property.owner, money);
            }
            else { // Adding houses
                let money = (house_amount - property.house_count) * property.house_value;
                if(money > property.owner.money) {
                    return;
                }
                this.transfer_money(property.owner, undefined, money);
            }
        }
        property.house_count = house_amount;
        this.broadcast({type:'property', house_count: house_amount, location:property.location});
    }

    /**
     * 
     * @param {Player} player 
     * @param {number} location
     * @param {boolean} active
     */
    mortgage_property(player, location, active) {
        let property = /** @type {Property} */ (this.board.get(location));
        if(!property || property.constructor.name !== 'Property' || property.owner !== player) {
            return;
        }

        let can_mortgage = true;
        for(const p of this.board.get_street_group(property.group)) {
            if(p.house_count > 0) {
                can_mortgage = false;
                break;
            }
        }
        if(!can_mortgage) {
            return;
        }

        if(active) { // Mortgaging property
            if(property.mortgaged) {
                return;
            }
            property.mortgaged = true;
            this.broadcast({type: 'property', mortgaged: true, location:property.location});
            this.transfer_money(undefined, player, property.mortgage_value);
        }
        else { // Lifting mortgage
            if(!property.mortgaged) {
                return;
            }
            if(player.money < property.mortgage_value * 1.1) {
                return;
            }
            property.mortgaged = false;
            this.broadcast({type: 'property', mortgaged: false, location:property.location});
            this.transfer_money(player, undefined, Math.floor(property.mortgage_value * 1.1));
        }
    }

    /**
     * 
     * @param {Player} recv 
     * @param {Player} send 
     */
    apply_trade(recv, send) {
        const trade = recv.current_trade;
        if(trade.money > 0 ? send.money < trade.money : recv.money < -1 * trade.money) {
            return;
        }
        for(const pId of trade.send.properties) {
            let property = /** @type {Property} */ (this.board.get(pId));
            if(property.owner !== send) {
                return;
            }
            for(const prop of this.board.get_street_group(property.group)) {
                if(prop.house_count > 0) {
                    return;
                }
            }
        }
        for(const pId of trade.recv.properties) {
            let property = /** @type {Property} */ (this.board.get(pId));
            if(property.owner !== recv) {
                return;
            }
            for(const prop of this.board.get_street_group(property.group)) {
                if(prop.house_count > 0) {
                    return;
                }
            }
        }
        
        this.transfer_money(trade.money > 0 ? send : recv, trade.money < 0 ? send : recv, Math.abs(trade.money));
        for(const pId of trade.send.properties) {
            let property = /** @type {Property} */ (this.board.get(pId));
            this.grant_property(property, send, recv, 0);
        }
        for(const pId of trade.recv.properties) {
            let property = /** @type {Property} */ (this.board.get(pId));
            this.grant_property(property, recv, send, 0);
        }

        recv.clear_trade();
        send.clear_trade();
    }


    /**
     * @param {string} msg
     */
    send_message(msg) {
        this.lobby.send_message('Game', msg);
    }

    broadcast(data) {
        this.players.forEach((player) => {
            player.emit('game', data);
        });
    }
    
    /**
     * 
     * @param {Player} player 
     * @param {string} deckname 
     */
    pick_card(player, deckname){
        if(!this.board.decks[deckname]) {
            return;
        }
        let card = this.board.decks[deckname].pick_card();
        if(card && card.action) {
            this.send_message(player.get_name() + ' has picked a ' + deckname + ' card: ' + card.description);
            card.action(player);
        }
    }
}