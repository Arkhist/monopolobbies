import Turn from './Turn.js';

/** @typedef {import('./Player.js').default} Player */
/** @typedef {import('./Game.js').default} Game */


export default class Space {
    
    name = '';
    /** @type {(turn: Turn, space: Space, player: Player) => void} */
    on_enter = (turn, space, player) => { turn.on_round_end(); };
    /** @type {string} */
    description = '';

    /** @type {number} */
    location = null;

    
    /**
     * @virtual
     * @return {Space}
     */
    copy() {
        let space = new Space();
        this.copy_metadata(space);
        return space;
    }

    /**
     * @param {Space} space
     */
    copy_metadata(space) {
        space.name = this.name;
        space.description = this.description;
        space.on_enter = this.on_enter;
        space.location = this.location;
    }

    /**
     * @virtual
     * @param {Game} game
     */
    get_display_data(game) {
        return {
            name: this.name,
            description: this.description
        };
    }
};