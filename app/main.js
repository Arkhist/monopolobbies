"use strict";

import escape from 'escape-html';
import express from 'express';
import expressSession from 'express-session';
const staticMiddleware = express.static;
import httpModule from 'http';
import socketIo from 'socket.io';

import { dirname } from 'path';
import { fileURLToPath } from 'url';

import Server from './Server.js';

// @ts-ignore
const DIRNAME = dirname(fileURLToPath(import.meta.url));


const expApp = express();
expApp.use(express.json());
expApp.use(express.urlencoded({ extended: true }));
expApp.set('view engine', 'ejs');
expApp.set('views', DIRNAME + '/html');

const sessionMiddleware = expressSession({
    secret: 'Monopolobbies is cool',
    resave: false,
    saveUninitialized: true
  });
expApp.use(sessionMiddleware);

expApp.use('/lib', staticMiddleware(DIRNAME + '/html/static/lib'));
expApp.use('/style', staticMiddleware(DIRNAME + '/html/static/style'));


const http = httpModule.createServer(expApp);
const io = socketIo(http);
io.use(function (socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
  });

const server = new Server(expApp, io, http);

// Start game server
server.start_server();
