import Card, {get_card} from './Card.js'
import fs from 'fs'

import { dirname } from 'path';
import { fileURLToPath } from 'url';



// @ts-ignore
const DIRNAME = dirname(fileURLToPath(import.meta.url));

let decks_stored = {};

/**
* 
* @param {string} name Name of the deck
* @returns {Deck} Generated Deck
*/
function generate_deck(name) {
    let deck_data;
    {
        let deckfd = fs.openSync(DIRNAME + "/decks/" + name + "/deck.json", 'r');
        if(deckfd == -1) {
            console.log("Error while generating deck "+name+": /decks/"+name+"/deck.json not opened.");
            return undefined;
        }
        
        let deck_content = fs.readFileSync(deckfd);
        fs.closeSync(deckfd);
        deck_data = JSON.parse(deck_content.toString());
    }
    if(deck_data.cards) {
        let deck = new Deck();
        for(let i = 0; i < deck_data.cards.length; i++) {
            let card = get_card(name, deck_data.cards[i].name);
            if(!deck_data.cards[i].amount)
            deck_data.cards[i].amount = 1;
            for(let j = 0; j < deck_data.cards[i].amount; j++)
            deck.cards.push(card);
        }
        
        decks_stored[name] = deck;
        return deck;
    }
    return undefined;
}

/**
* 
* @param {string} name 
*/
export function get_deck(name) {
    if(!decks_stored[name])
    generate_deck(name);
    return decks_stored[name].copy();
}

export default class Deck {
    
    /** @private @type {Card[]} */
    cards = [];
    current_card = 0;
    
    constructor() {
        
    }
    
    /**
    * @returns {Deck}
    */
    copy() {
        let deck = new Deck();
        for (let i = 0; i < this.cards.length; i++) {
            deck.cards[i] = this.cards[i];
        }
        return deck;
    }
    
    shuffle() {
        let tmp, i, m = this.cards.length;
        
        while(m) {
            i = Math.floor(Math.random() * m--);
            
            tmp = this.cards[i];
            this.cards[i] = this.cards[m];
            this.cards[m] = tmp;
        }
    }
    
    /**
    * @returns {Card}
    */
    pick_card() {
        let card = this.cards[this.current_card];
        
        this.current_card++;
        if (this.current_card >= this.cards.length) {
            this.current_card = 0;
            this.shuffle();
        }
        return card;
    }
    
}