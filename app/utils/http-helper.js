export function set_bad_request_response(res) {
    res.statusCode = 400;
    res.statusMessage = 'Bad request';
    res.end();
}