import Stage from './Stage.js'

import GameStage from './GameStage.js'
import ChatStage from './ChatStage.js'
import HomeStage from './HomeStage.js'

/** @typedef {import('../Client.js').default} Client */
/** @typedef {import('../Lobby.js').default} Lobby */
/** @typedef {import('../Game.js').default} Game */

export default class LobbyStage extends Stage {
    
    /** @private @type {Lobby} */
    lobby;

    /** @private @type {GameStage} */
    game_stage;

    listeners = {
        socket: [(data) => this.socket_listener(data)],
        request: [(requestBody, res) => this.request_listener(requestBody, res)]
    };

    /**
     * @param {Client} client
     * @param {Lobby} lobby
     */
    constructor(client, lobby) {
        super('lobby', undefined, client);
        this.lobby = lobby;
    }

    enter() {
        if (!this.client.chatHandler) {
            this.client.chatHandler = new ChatStage(this.client);
            this.client.subscribe(this.client.chatHandler);
            this.client.chatHandler.enter();
        }
        this.lobby.add_client(this.client);
        this.client.chatHandler.join_channel('lobby-'+this.lobby.id);
    }

    leave() {
        if(this.game_stage) {
            this.game_stage.leave();
            this.client.unsubscribe(this.game_stage);
        }
        this.client.chatHandler.leave_channel('lobby-'+this.lobby.id);
        this.lobby.remove_client(this.client);
    }

    socket_listener(data) {
        if(data.type === 'sync-request') {
            this.on_lobby_update(this.lobby.get_update_data());
        }
        else if(data.type === 'edit') {
            if(this.client !== this.lobby.owner) {
                return;
            }
            if(!data.key || typeof data.value !== 'string') {
                return;
            }
            if(data.key === 'name') {
                if(data.value.length < 4 || data.value.length > 100) {
                    return;
                }
                this.lobby.room_config.name = data.value;
                this.lobby.lobby_updated();
            }
            else if(data.key === 'password') {
                if(data.value.length > 64) {
                    return;
                }
                this.lobby.room_config.password = data.value;
                this.lobby.lobby_updated();
            }
        }
        else if(data.type === 'start') {
            if(this.client !== this.lobby.owner) {
                return;
            }
            this.lobby.start_game();
        }
    }

    request_listener(requestBody, res) {
        if (requestBody.type !== this.get_event_name()) {
            return false;
        }
        if(requestBody.action === 'leave') {
            this.client.set_stage(new HomeStage(this.client));
            res.send({ok:true});
            return true;
        }
        return true;
    }

    /**
     * @param {*} update_data
     */
    on_lobby_update(update_data) {
        this.client.emit('lobby', {type:'update', data:update_data, myself:this.client.id});
    }

    /**
     * @param {Game} game
     */
    game_start(game, player) {
        if(!this.game_stage) {
            this.game_stage = new GameStage(this.client, this, game, player);
            this.client.subscribe(this.game_stage);
        }
    }

    game_over() {
        if(this.game_stage) {
            this.game_stage.leave(true);
            this.client.unsubscribe(this.game_stage);
            this.game_stage = null;
        }
    }

    get_home_url() {
        if(this.lobby.running) {
            return '/game/'+this.lobby.id;
        }
        return '/lobby/'+this.lobby.id;
    }
}
