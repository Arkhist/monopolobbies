/** @typedef {import('../Client.js').default} Client */

export default class Stage {

    /** @private @type {string} */
    event_name;
    /** @private @type {string} */
    home_url;

    /** @private @type {Client} */
    client;

    listeners = {
        socket: [],
        request: []
    };

    /**
     * @param {string} event_name
     * @param {string} home_url
     * @param {Client} client
     */
    constructor(event_name, home_url, client) {
        this.event_name = event_name;
        this.home_url = home_url;
        this.client = client;
    }

    /**
     * @virtual
     * Called when Stage is entered
     */
    enter() {}

    /**
     * @virtual
     * Called when Stage is left
     */
    leave() {}

    get_event_name() {
        return this.event_name;
    }

    get_home_url() {
        return this.home_url;
    }
}