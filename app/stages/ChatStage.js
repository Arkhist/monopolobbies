import Stage from './Stage.js'

import { ChatChannel } from '../Chat.js'

/** @typedef {import('../Player.js').default} Player */
/** @typedef {import('../Client.js').default} Client */

/** @typedef {{author:string, message:string}} ChatMessage */


export default class ChatStage extends Stage {
    
    listeners = {
        socket: [(data) => this.socket_listener_chat(data)],
        request: []
    };

    /** @type {ChatChannel[]} */
    channels = [];

    /**
     * 
     * @param {Client} client 
     */
    constructor(client) {
        super('chat', undefined, client);
    }

    enter() {
        this.join_channel('global');
    }

    /**
     * @param {string | ChatChannel} channel
     */
    join_channel(channel) {
        let channel_obj;
        if(typeof channel === 'string') {
            channel_obj = this.client.server.get_channel(channel);
        }
        else {
            channel_obj = (/** @type {ChatChannel} */ (channel));
        }

        if(!channel_obj) {
            return;
        }
        channel_obj.add_listener(this);
        this.channels.push(channel_obj);
    }

    leave() {
        for(const channel of this.channels)
            this.leave_channel(channel)
    }

    /**
     * @param {string | ChatChannel} channel
     */
    leave_channel(channel) {
        let channel_obj;
        if(typeof channel === 'string') {
            channel_obj = this.client.server.get_channel(channel);
        }
        else {
            channel_obj = (/** @type {ChatChannel} */ (channel));
        }

        if(!channel_obj) {
            return;
        }
        channel_obj.remove_listener(this);
        this.channels = this.channels.filter((chan) => chan !== channel_obj);
    }

    /**
     * @param {string} name
     */
    get_channel_from_name(name) {
        let channel = this.channels.find((val) => val.name === name);
        return channel;
    }

    socket_listener_chat(data) {
        if(!data.type)
            return;
        if(data.type === 'message') {
            if(data.channel && data.message) {
                let channel = this.get_channel_from_name(data.channel);
                if(channel) {
                    channel.add_message({author: this.client.username, message: data.message});
                }
            }
        }
        else if(data.type === 'sync-request') {
            let messages = {};
            for(const channel of this.channels) {
                messages[channel.name] = {
                    name: channel.display_name,
                    history: channel.get_history()
                };
            }
            this.client.emit('chat', {type:'sync-request', channels:messages});
        }
    }

    /**
     * @param {string} channel
     * @param {ChatMessage} message
     */
    on_chat_message(channel, message) {
        this.client.emit('chat', {channel: channel, message: message});
    }
}