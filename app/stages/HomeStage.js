import Stage from './Stage.js'

import ChatStage from './ChatStage.js'
import LobbyStage from './LobbyStage.js'

/** @typedef {import('../Client.js').default} Client */

export default class HomeStage extends Stage {

    listeners = {
        socket: [(data) => this.socket_listener(data)],
        request: [(requestBody, res) => this.request_listener(requestBody, res)]
    };

    /**
     * @param {Client} client
     */
    constructor(client) {
        super('home', '/home', client);
    }

    enter() {
        if (!this.client.chatHandler) {
            this.client.chatHandler = new ChatStage(this.client);
            this.client.subscribe(this.client.chatHandler);
            this.client.chatHandler.enter();
        }
        this.client.server.add_home_listener(this);
    }
    
    leave() {
        this.client.server.remove_home_listener(this);
    }

    on_lobby_update(lobbyData) {
        this.client.emit('home', {type:'lobby-update', data:lobbyData});
    }

    request_listener(requestBody, res) {
        if (requestBody.type !== this.get_event_name()) {
            return false;
        }
        if(requestBody.action === 'create-lobby') {
            let lobby = this.client.server.create_lobby(this.client);
            if(!lobby) {
                res.redirect('/home');
                res.end();
                return true;
            }

            this.client.set_stage(new LobbyStage(this.client, lobby));
            res.redirect('/lobby/'+lobby.id);
            res.end();
            return true;
        }
        else if(requestBody.action === 'join-lobby') {
            if(typeof requestBody.id !== 'number' || typeof requestBody.password !== 'string') {
                res.send({ok:false, error:'Invalid data provided'});
                return true;
            }
            let lobby = this.client.server.get_lobbies().find((val) => val.id === requestBody.id);
            if(!lobby) {
                res.send({ok:false, error:'This lobby is unavailable.'});
                return true;
            }
            if(lobby.running) {
                res.send({ok:false, error:'This lobby is currently busy.'});
                return true;
            }
            if(lobby.clients.length >= 6) {
                res.send({ok:false, error:'This lobby is full.'});
                return true;
            }
            if(lobby.room_config.password !== requestBody.password) {
                res.send({ok:false, error:'Invalid password.'});
                return true;
            }
            this.client.set_stage(new LobbyStage(this.client, lobby));
            res.send({ok:true});
            return true;
        }

        return false;
    }

    socket_listener(data) {
        if(!data.type) {
            return;
        }
        if(data.type === 'lobby-request') {
            let lobby_data = [];
            let lobbies = this.client.server.get_lobbies();
            for(const lobby of lobbies) {
                if(!lobby.running) {
                    lobby_data.push(lobby.get_display_data());
                }
            }
            this.client.emit('home', {type:'lobby-request', lobbies:lobby_data});
        }
    }
}