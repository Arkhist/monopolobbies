import Stage from './Stage.js'
import { PlayerStatus } from '../Player.js';

/** @typedef {import('./LobbyStage.js').default} LobbyStage */
/** @typedef {import('../Game.js').default} Game */
/** @typedef {import('../Player.js').default} Player */
/** @typedef {import('../Client.js').default} Client */
/** @typedef {import('socket.io').Socket} SocketClient */


export default class GameStage extends Stage {

    listeners = {
        socket: [(data, socket) => this.socket_listener(data, socket)],
        request: []
    };

    /** @type {LobbyStage} */
    lobby_stage;

    /** @type {Game} */
    game;

    /** @type {Player} */
    player;

    /**
     * @param {Client} client
     * @param {LobbyStage} lobby_stage
     */
    constructor(client, lobby_stage, game, player) {
        super('game', undefined, client);
        this.lobby_stage = lobby_stage;
        this.game = game;
        this.player = player;
    }

    /**
     * @param {any} data
     * @param {SocketClient} socket
     */
    socket_listener(data, socket) {
        if(data.type === 'sync-request') {
            if(data.step === 'board') {
                socket.emit('game', {type: 'sync-request', step: 'board', data: this.game.board.get_display_data(this.game)});
            }
            else if(data.step === 'players') {
                socket.emit('game', {type: 'sync-request', step: 'players', data: this.game.get_players_display(), myself: this.client.id});
            }
            else if(data.step === 'state') {
                let display_data = this.game.current_turn.get_display_data();
                display_data.current_trade = this.player.current_trade;
                socket.emit('game', {type: 'sync-request', step: 'state', data: display_data});
            }
        }
        else if(data.type === 'action') {
            if(this.game) {
                this.game.handle_action(this.player, data.action);
            }
        }
    }

    enter() {

    }

    /**
     * @param {boolean} [game_over]
     */
    leave(game_over) {
        if(game_over) {
            this.player.status = PlayerStatus.OUT;
        }
        else {
            this.game.set_bankrupt(this.player);
        }
        this.player = null;
        this.game = null;
    }
}