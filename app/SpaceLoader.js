/** @typedef {import('./Player.js').default} Player */
/** @typedef {import('./Turn.js').default} Turn */


import Space from './Space.js';
import Property, { PropertyType } from './Property.js';

import fs from 'fs';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

// @ts-ignore
const DIRNAME = dirname(fileURLToPath(import.meta.url));

/**
 * @param {(player: Player) => number | number} tax_amount
 * @returns {(turn: Turn, space: Space, player: Player) => void}
 */
function gen_action_tax(tax_amount) 
{
    return  /** @param {Turn} turn @param {Space} space @param {Player} player */ (turn, space, player) => {
        let tax_amt = (typeof tax_amount === "function") ? tax_amount(player) : tax_amount;
        player.game.transfer_money(player, undefined, tax_amt);
        turn.on_round_end();
    };
}
/**
 * @returns {(turn: Turn, space: Space, player: Player) => void}
 */
function gen_action_park() {
    return /** @param {Turn} turn @param {Space} space @param {Player} player */ (turn ,space, player) => {
        //player.game.transfer_from_bank(player);
    }
}

/**
 * @param {string} deckname
 * @returns {(turn: Turn, space: Space, player: Player) => void}
 */
function gen_action_draw(deckname) {
    return /** @param {Turn} turn @param {Space} space @param {Player} player */ (turn, space, player) => {
        player.game.pick_card(player, deckname);
        turn.on_round_end();
    }
}

/**
 * @returns {(turn: Turn, space: Space, player: Player) => void}
 */
function gen_action_jail() {
    return /** @param {Turn} turn @param {Space} space @param {Player} player */ (turn ,space, player) => {
        player.game.jail(player);
    }
}



/**
 * @param {string} board_name
 * @param {string} name
 */
function generate_space(board_name, name){
    let space_data;
    try {
        let space_fd = fs.openSync(DIRNAME + '/boards/' + board_name + '/' + name + '.json', 'r');
        if(space_fd <= -1){
            console.log('Error while generating space ' + name + ': /boards/'+board_name+'/'+name+'.json not opened');
            return undefined;
        }

        let space_content = fs.readFileSync(space_fd);
        fs.closeSync(space_fd);
        space_data = JSON.parse(space_content.toString());
    }
    catch(err){
        console.log('Error while generating space '+name+': '+err);
        return undefined;
    }
    let space;
    if (space_data.property !== undefined) {
        space = get_property(space_data.property);
    }
    else {
        space = new Space();
    }
    space.description = space_data.description;
    space.name = space_data.name;
    if (space_data.action){
        if(space_data.action === 'draw') {
            space.on_enter = gen_action_draw(space_data.deck);
        }
        else if(space_data.action === 'tax') {
            space.on_enter = gen_action_tax(space_data.amount);
        }
        else if(space_data.action === 'jail') {
            space.on_enter = gen_action_jail();
        }
    }

    return space;
}

export function get_space(board_name, name) {
    return generate_space(board_name, name);
}



/**
 * @return {Property}
 */
function generate_property(data) {
    let property = new Property();
    if(data.house_value !== undefined) {
        property.house_value = data.house_value;
    }
    if(data.value !== undefined) {
        property.value = data.value;
    }
    if(data.rent !== undefined) {
        property.rent = data.rent;
    }
    if(data.property_type !== undefined) {
        switch(data.property_type) {
            case 'street':
                property.property_type = PropertyType.STREET;
                break;
            case 'railroad':
                property.property_type = PropertyType.RAILROAD_STATION;
                break;
            case 'utility':
                property.property_type = PropertyType.UTILITY;
                break;
            default:
                property.property_type = -1;
                break;
        }
    }
    if(data.mortgage_value !== undefined) {
        property.mortgage_value = data.mortgage_value;
    }
    if(data.group !== undefined) {
        property.group = data.group;
    }
    return property;
}

/**
 * @return {Property}
 */
export function get_property(data) {
    return generate_property(data);
}